<?php
date_default_timezone_set( 'Europe/Oslo' );

function getDates($week, $year) {
  $dto = new DateTime();
  $dto->setISODate($year, $week);
  $ret = [];
  $ret[] = $dto->format('Y-m-d');
  $dto->modify('+6 days');
  $ret[] = $dto->format('Y-m-d');
  return $ret;
}
for ( $i = 1; $i <= 52; $i++ ) {
  $_file = 'hours/'. date( 'Y' ) ."/$i.txt";
  if ( file_exists( $_file ) ) {
    if ( ! trim( file_get_contents( $_file ) ) ) {
      unlink( $_file );
    }
  }
}

$file = @$_GET['file'];
if ( !$file ) {
  $path = __DIR__.'/hours/'. date( 'Y' );
  if ( ! file_exists( $path ) ) {
    `mkdir -p "$path"`;
  }
  $file = $path .'/'. date('W') .'.txt';
}
if ( ! file_exists( $file ) ) {
  touch( $file );
}
function getTitle( $file ) {
  if ( preg_match('/(\d{4})\/(\d+)\.txt$/', $file, $matches ) ) {
    $dates = getDates( $matches[2], $matches[1] );
    return "{$dates[0]} to {$dates[1]}";
  } else {
    return $file;
  }
}

if ( isset( $_POST['text'] ) ) {
  if ( !is_writable( $file ) )
    @chmod( $file, 666 );
  if ( !is_writable( $file ) )
    die( 'error' );
  $written = file_put_contents( $file, $_POST['text'] );
  if ( !$written )
    die('error');

  $dir = 'revision';
  if ( !is_dir( $dir ) )
    mkdir( $dir );
  $dir .= '/'. date( 'Y' );
  if ( !is_dir( $dir ) )
    mkdir( $dir );
  $dir .= '/month';
  if ( !is_dir( $dir ) )
    mkdir( $dir );
  $dir .= '/' .date( 'm' );
  if ( !is_dir( $dir ) )
    mkdir( $dir );

  $dir .= '/';
  file_put_contents( $dir.date('Y-m-d\TH:i').'.txt', $_POST['text'] );
}
$liste = file_get_contents( $file );

if ( isset( $_GET['refresh'] ) )
  die( $liste );

if ( isset( $_POST['text'] ) ) {
  die( 'ok' );
}

$projects = json_decode( file_get_contents( 'projects.json' ), 1 );
if ( ! $projects ) {
  die( "Could not load projects.json" );
}

$new_json = FALSE;
if ( @$projects['json-url'] ) {
  $new_json = file_get_contents( $projects['json-url']);
  if ( json_decode( $new_json ) ) {
    $saved = file_put_contents( 'projects.json', $new_json );
    if ( !$saved )
      die( 'projects.json needs to be writeable' );
  }
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title><?= getTitle( $file ); ?></title>
  <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="context-awareness.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
  <?php if ( file_exists( 'plugin.css' ) ): ?>
    <link rel="stylesheet" type="text/css" href="plugin.css">
  <?php endif; ?>
  <style type="text/css">

    @font-face {
      font-family: "typed";
      src: url("/assets/cmcfdbi.woff2"),
           url("/assets/cmcfdbi.woff");
    }

  .annoying-popover {
    position: fixed;
    top: 0; bottom: 0;
    right: 0; left: 0;
    background: rgba( 255,255,255, 0.9 );
    z-index: 100000;
    display: flex;
    text-align: center;
    align-items: center;
    justify-content: center;
  }


  .annoying-box {
    box-shadow: 0 5px 20px -10px rgba(0,0,0,0.6);

    width: 100%;
    height: 80vh;

    max-width: 60rem;
    max-width: 80vw;

    background: white;
    padding: 5rem;
    border-radius: 4px;

    display: flex;
    flex-direction: column;
    align-items: center;
    flex-wrap: wrap;
    justify-content: center;
  }

  .annoying-popover .message {
    font-family: "typed", monospace;
    font-size: 3.75rem;
    line-height: 1.5;
    color: black;
    margin-bottom: 2rem;
    letter-spacing: -1px;
  }

  .annoying-popover .message small {
    color: grey;
    font-size: 2rem;
    letter-spacing: 0;
  }

  .instructions{
    border-top: 1px solid lightgrey;
    padding-top: 1rem;
  }
.ambulance {
  animation: police .5s infinite;
  animation-direction: alternate;
}
@keyframes police {
  0% {background-color: rgba(255,255,255,0.9);}
  50% {background-color: rgba(255,100,100,1);}
  100% {background-color: rgba(255,255,255,0.9);}
}
body, body::before, body .wrapper {
   /*-webkit-transform: translateZ(0);
   transform: translateZ(0);*/
}
body::before {
  background: linear-gradient( rgba(4, 204, 204, 0.2), rgba(158, 4, 255, 0.3) ), url('//unsplash.it/1660/1080?random') 100% 100%;
  background-size: cover;
  background-blend-mode: screen;
  content: '';
  width: 100%;
  height: 100%;
  position: absolute;
  transition: all 250ms cubic-bezier(0.77, 0, 0.175, 1);;
  -webkit-filter: blur(0px);
}
/* */
body.focused:before {
  -webkit-filter: blur(3px);
}

body .input,
body .output{
  border-left-color: transparent;
  transition: all 500ms cubic-bezier(0.77, 0, 0.175, 1) 200ms;

  width: 60%;
}
.context {
  width: 40%;
  position: fixed;
  top: 0; right: 0;
  height: 100%;
  background: white;
  opacity: 0.5;
  overflow: scroll;
  padding-top: 4rem;
}
body.focused .input,
body.focused .output{
  border-left-color: #1A1E23;
}

body .wrapper {
  opacity: 0.2;
  transition: all 1.5s cubic-bezier(0.77, 0, 0.175, 1);
  box-shadow: inset 0 0 10rem 0rem rgba(26, 30, 35, 0.3);
  /*-webkit-filter: blur(3px);*/
}
/* Fade in a dark background for working*/
body.focused .wrapper {
  opacity: 1;
  /*-webkit-filter: blur(0px);*/
  /*background: rgba(26, 30, 35, 0.81);*/
  box-shadow: inset 0 0 10rem 200rem rgba(26, 30, 35, 0.8);
}

.links {
  opacity: 0;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  width: 7rem;
  z-index: 10000;
  list-style: none;
  font-size: 12px;
  margin: 0;
  padding: 0;
  text-align: center;
  transition: all 0.15s;
  background: rgb(255, 234, 218);
  overflow: scroll;
  font-weight: 600;
}
.links:hover {
  opacity: 1;
}
.links li {
  padding: 0;
  margin: 0;
}
.links li a, .links li span {
  display: block;
  padding: 0.25rem 1rem;
  width: 100%;
}
.links li a:hover {
  background: white;
}

.no-file {
  color: green;
}
.this-week {
  color: red;
}

</style>
</head>
<body class="focused">
<ul class="links">
<?php
  for ( $i = 1; $i <= 52; $i++ ) {
    echo "<li>";
    $_file = 'hours/'. date( 'Y' ) ."/$i.txt";
    if ( file_exists( __DIR__."/$_file" ) && date( 'W' ) == $i ) {
      echo "<a class=\"this-week\" href=\"?file=$_file\">$i</a>";
    } else if ( file_exists( __DIR__."/$_file" ) ) {
      echo "<a href=\"?file=$_file\">$i</a>";
    } else if ( $i < date( 'W' ) ) {
      echo "<a class=\"no-file\" href=\"?file=$_file\">$i</a>";
    } else {
      echo "<span>$i</span>";
    }
    echo "</li>";
  }
?>
</ul>
<div class="header"><h1><?= getTitle( $file ); ?></h1></div>
<div class="annoying-popover" style="display:none">
  <div class="annoying-box">
    <p class="message">
      <?php
        $messages = [
          "🆘 🆘 🆘<br />...it's been a long while<br />since you last exported...",
          "💳 💵 💷<br />You like money,<br/>don't you?<br />Export to get paid.",
          "😦 😦 😦<br />oh m8...<br />Export your damn hours...",
          "🤔<br />Trimpletext?<br />That can't be right...",
          "⏲ Time is money 💰",
          "Hey, listen!<br><br>...Tripletex!",
          "<img src='/assets/logging.gif' style='width: 350px; height: auto; margin-bottom: 1rem' alt=OI!!!' /><br />Export.🕑 Your.🕓 Hours.🕗",
          "<img src='/assets/logging.gif' style='width: 200px; height: auto; margin-bottom: 1rem' alt=OI!!!' /><br />⏰ ⏰ ⏰<br />Export your hours!",
        ];
      ?>
      <?= $messages[array_rand($messages)]; ?>
    </p>
    <p class="instructions">Click anywhere to continue logging your hours</p>
  </div>
</div>
<script type="text/javascript">
  $( '.annoying-popover' ).click( function() {
    $( this ).fadeOut( 1000 );
    $( 'textarea' ).focus();
  } );
  var le = localStorage.last_export;
  if ( typeof le === 'undefined' ) {
    le = 0;
  }
  if ( (new Date() ).getTime() - parseInt( le, 10 ) > 48 * 60 * 60 * 1000 ) {

    if ( Math.random() > 0.9 ) {
      $( '.annoying-popover' ).show();
      if ( Math.random() > 0.7 ) {
        $( '.annoying-popover' ).addClass( 'ambulance' );
      }
    }
  }
</script>
<div class="wrapper">
  <div class="context">Hello world</div>
  <div class="output-outer">
    <div class="output"><span class="warning">Please enable javascript</span><br><?php echo $liste; ?></div>
  </div>
  <textarea class="input" spellcheck="false"><?php echo $liste; ?></textarea>
</div>
<textarea class="parsed" spellcheck="false">Sample text</textarea>
<a target="_blank" href="https://tripletex.no/execute/updateHourlist?contextId=2776925"><button class="export-btn">Export</button></a>
<script type="text/javascript">
  $( document ).ready( function() {
    var issues = {};
    var update_output = function( content ) {
      output.html( process_text( content ) );
      output.find( '.day-start' ).each( function() {
        var tm = 0;
        $( this ).find( '.time' ) .each( function() {
          var hm = $( this ).data( 'hm' );
          if ( ! hm ) {
            return;
          }
          tm += parseInt( $( this ).data( 'hm' ), 10 );
        } );
        var total = $( '<span class="total">' );
        var hours = Math.floor( tm / 60);
        var minutes = tm % 60;
        total.text( hours +'t/'+ minutes +'m' );
        $( this ).find( '.date' ).after( ' ', total );
      } );
    }
    var check_permissions = function( callback ) {
      if ( ! window.Notification ) {
        return;
      }
      // Let's check whether notification permissions have already been granted
      if ( Notification.permission === 'granted' ) {
        callback();
      }
      // Otherwise, we need to ask the user for permission
      else if ( Notification.permission !== 'denied' ) {
        // Run the callback when permission is granted or denied
        Notification.requestPermission().then( callback );
      }
    }

    var should_remind = true;
    var remind_hours = function() {
      if ( should_remind ) {
        var notification = new Notification( 'Hi there', {
          icon: '//unsplash.it/160/160?random',
          body: 'You\'ve not logged your hours in a while',
          tag: 'hourlogger',
          renotify: true,
          sticky: true,
          requireInteraction: true
        } );

        notification.onclick = function() {
          notification.close();
          $( '.annoying-popover' ).hide();
          window.focus();
          textarea.focus();
        }
      }
      should_remind = true;
    }
    var start_reminding = function() {
      setInterval( remind_hours, 20 * 60 * 1000 );
    }

    check_permissions( start_reminding );

    var colour_line = function( line ) {
      var url;
      var matches;

      // Code matches
      matches = line.match( / [a-z]+\#\d+/g );
      if ( matches ) {
        for ( var index in matches ) {
          var parts = matches[index].match( /([a-z]+)\#(\d+)/ );
          var key = parts[0];
          url = issue_tracking[ parts[1] ];
          if ( url ) {
            url += parts[2];
            line = line.replace( key, '<a class="code" target="_blank" href="'+ url +'">'+ key +'</a>' );
          }
        }
      }
      matches = line.match( / ([A-Z]+\-\d+)/ );
      if ( matches ) {
        url = 'https://jira.driv.digital/browse/';
        line = line.replace( / ([A-Z]+\-\d+)/g, ' <a class="code" target="_blank" href="'+ url +'$1">$1</a>' );
      }
      // URL matches
      matches = line.match( / (https?:\/\/[\S]+)/ );
      if ( matches ) {
        line = line.replace( / (https?:\/\/[\S]+)/g, ' <a class="link" target="_blank" href="$1">$1</a>' );
      }

      // Project matches
      matches = line.match( /^\d{4}\-\d* (\S+)/i );
      if ( matches && matches[1].toLowerCase() in projects ) {
        line = line.replace( /^(\d{4}\-\d*) (\S+)/i, '$1 <strong title="'+projects[matches[1]]+'">$2</strong>' );
      }

      return line;
    };
    var ot = $('h1').text();

    var days = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    var textarea = $( '.input' );
    var output = $( '.output' );
    var projects = {};
    var issue_tracking = {};
    var save_debouncer;
    var save_stuff = function() {
      $.post(
        '/?file=<?php echo $file; ?>',
        { text:$.trim( textarea.val() )+"\n" },
        function( message ) {
          if ( 'ok' != message ) {
            $( 'html' ).addClass( 'ajax-error' );
            $( 'h1' ).text( '<?php echo $file; ?>; Could not save file!' );
            console.log( message );
          }
          else {
            $( 'html' ).addClass( 'ajax-ok' );
            if ( $( 'html' ).hasClass( 'ajax-error' ) ) {
              $( 'html' ).removeClass( 'ajax-error' );
              $( 'h1' ).text( ot );
            }
          }
          $( 'html' ).removeClass( 'ajax-pending' );

          // Done saving hours to textfile
          // Parse the hours for good fun and waste some CPU :)
          $.get( '/parser.php', tmp_save_parsed_hours_to_textarea );
        }
      );
    };
    var process_text = function( text ) {
      $( 'html' ).addClass( 'ajax-pending' );
      if ( save_debouncer )
        save_debouncer = clearTimeout( save_debouncer );
      save_debouncer = setTimeout( save_stuff, 500 );

      var html = [];
      var day_started = false;
      var lines = text.split("\n");
      var time_line_nr = 0;
      for ( var l in lines ) {
        var time_html = '';
        var c = '';
        var line = lines[l];
        var time_matches = line.match( /^(\d{2})(\d{2})\-(\d{2})(\d{2})/ );
        if ( line.match( /^\#/ ) ) {
          // Comment line
          c = 'class=comment';
        }
        else if ( line.match( /^\d{4}\-\d{2}\-\d{2}$/ ) ){
          // Date line
          c = 'class=date';
          var _d = new Date( line );
          time_html = '<div class="day-start">';
          day_started = true;
          time_html += '<span class="time c5">'+ days[_d.getDay()].substr(0,3) +'</span>';

          if ( html.length ) {
            time_html = '</div>' + time_html;
          }
        }
        else if ( time_matches ) {
          // Time line
          c = 'class="line c' + ( time_line_nr % 4 + 1 ) + '"';
          var _h = time_matches[3] - time_matches[1];
          var _m = time_matches[4] - time_matches[2];
          if ( 0 > _m ) {
            _h -= 1; _m += 60;
          }
          var _hm = _h * 60 + _m;
          time_html = '<span data-hm="'+ _hm +'" class="time c' + ( time_line_nr % 4 + 1 ) + '">'+ _h +"t/"+ _m +'m</span>';
          time_line_nr++;
        }
        else if ( line.match( /^\d{4}\-/ ) ) {
          // In progress / Unknown
          c = 'class=in-progress';
        }
        else {
          // Invalid entry
          c = 'class=error';
        }

        line = colour_line( line );

        html.push( time_html + '<span '+ c +'>' + line + '</span>' );
      }
      if ( day_started ) {
        // End a started day
        html.push( '</div>' );
      }
      var h = Math.max( $( '.wrapper' ).height() - 100 , output.outerHeight() );
      textarea.height( h );


      return html.join("<br>");
    };

    // Update text on key strokes
    textarea.on( 'keyup', function() {
      update_output( $(this).val() );
    } );
    // Update text once at page load
    update_output( textarea.val() );

    // Get the projects file
    $.getJSON( '/projects.json', function( data ) {
      // Merge allowed and special projects to one array
      projects = {};
      for (var x in data.allowed_projects ) {
        var project = data.allowed_projects[x];
        var y = x.toLowerCase();
        if ( typeof project == 'object' ) {
          projects[y] = project.name;
        } else {
          projects[y] = project;
        }
      }
      for (var x in data.special_projects ) {
        projects[x] = data.special_projects[x];
      }
      issue_tracking = data.issue_tracking;
      update_output( textarea.val() );
    } );


    // Prevent CMD + S
    var lastKey;
    $( window ).bind( 'keydown',function(e){
      if( lastKey && ( (lastKey == 91 && e.keyCode == 83) || (lastKey == 83 && e.keyCode == 91) ) ){
        e.preventDefault();
        $( 'h1' ).text( 'It saves automatically you fool!' );
        setTimeout( function() {
          $( 'h1' ).text( ot );
        }, 2000 );
        return false;
      }
      lastKey = e.keyCode;
      // delay desktop notifications
      should_remind = false;
    } );

    // Sticky header
    $( window ).on( 'scroll', function() {
      if ( 100 < $(document).scrollTop() ) {
        $( '.header' ).addClass( 'sticky' );
      }
      else {
        $( '.header' ).removeClass( 'sticky' );
      }
    } );

    // Update hours when coming back to the window
    $( document ).on( 'visibilitychange', function( e ) {
      if ( 'visible' !== document.visibilityState ) {
        textarea.attr( 'disabled', 'disabled' );
        return;
      }
      $.get( '/?refresh=true&file=<?= $file; ?>', updated_hours );
    } );

    // Inject the new fresh data where it's needed
    var updated_hours = function( data ) {
      textarea.val( data );
      update_output( data );
      textarea.removeAttr( 'disabled' );
    };

    /**
     * Temporarily save parsed hours to a textarea
     * for later extraction when we click "export"
     */
    var tmp_save_parsed_hours_to_textarea = function( data ) {
      $( '.parsed' ).val( data );
    };


    // Handle clicks on the "export" button
    $( '.export-btn' ).on( 'click', function() {
      localStorage.last_export = ( new Date() ).getTime();
      $( '.parsed' )[0].select();
      if ( document.execCommand( 'copy' ) ) {
        $( 'html' ).addClass( 'export-success' );
        $( 'h1' ).text( 'Parsed js copied to clipboard' );
      }
      else {
        $( 'html' ).addClass( 'export-error' );
        $( 'h1' ).text( 'Error in copying parsed js' );
      }
      setTimeout( export_done, 1500 );
    } );
    var export_done = function() {
      $( 'html' ).removeClass( 'export-success export-error' );
      $( 'h1' ).text( ot );
    };

    var get_now = function( hoff, moff ) {
        var now = new Date();
        var hh = now.getHours();
        if ( hoff )
          hh += hoff;
        var mm = now.getMinutes();
        if ( moff )
          mm += moff;
        mm = Math.round( mm / 15 ) * 15;
        while ( mm >= 60 ) {
          mm -= 60;
          hh += 1;
        }
        // Leading zeros
        hh = ( '0' + hh ).slice( -2 );
        mm = ( '0' + mm ).slice( -2 );
        return hh+mm;
    };
    var autocomplete = function( line, context ) {
      var now;
      if ( !line && !context.previous_line ) {
        now = new Date();
        var yyyy = now.getFullYear().toString();
        var mm = (now.getMonth() + 1).toString();
        var dd = now.getDate().toString();
        mm = ( '0' + mm ).slice( -2 );
        dd = ( '0' + dd ).slice( -2 );
        return {
          line:
             yyyy+"-"+mm+"-"+dd+ "\n",
          ss: 11,
          se: 11
        };
      }
      var matches;
      if ( !line && context.previous_line ) {
        matches = context.previous_line.match( /^\d{4}\-(\d{4}) / );
        var val = get_now();
        if ( matches )
          val = matches[1];
        return {
          line: val + '-',
          ss: 5,
          se: 5,
        };
      }
      if ( line.match( /^\d{4}\- / ) || line.match( /^\d{4}\-$/ ) ) {
        now =  get_now();
        if ( context.next_line.match(/^\d{4}\-\d{4}/) ) {
          now = context.next_line.substr( 0, 4 );
        }
        else if ( line.substr(0,4) == now ) {
          now =  get_now( 0, 15 );
        }
        return {
          line: line.substr( 0, 5 ) + now + line.substr( 5 ),
          ss: 4 + context.ss,
          se: 4 + context.se,
        };
      }
      else if ( line.match( /^\d{4}\-/ ) && context.ss < 5 && context.se < 5  ) {
        return {
          line: get_now() + line.substr( 4 ),
          ss: context.ss,
          se: context.se,
        };
      }
      else if ( line.match( /[A-Z]+\-\d+$/ ) && context.ss > 9 && context.se > 9 ) {
        matches = line.match( /[A-Z]+\-\d+$/ );
        return {
          line: line + ' {{' + matches[0] + '}}',
          ss: context.ss,
          se: context.se,
        };
      }
      return false;
    };

    var move_line = function( direction, e ) {
      // Both keys must be pressed
      if ( !( e.ctrlKey && e.metaKey ) )
        return;
      var lc = get_line_context();

    };
    var get_line_context = function() {
      var obj = {
        ss: textarea.get(0).selectionStart,
        se: textarea.get(0).selectionEnd,
        content: textarea.val()
      };
      obj.ls = obj.content.lastIndexOf( "\n", obj.ss-1 );
      obj.ls++;
      obj.le = obj.content.indexOf( "\n", obj.se );
      if ( -1 === obj.le )
        obj.le = obj.content.length;
      obj.line = obj.content.substr( obj.ls, obj.le-obj.ls );

      // Get the previous line (previous_line)
      obj.previous_line = '';
      if ( obj.ls ) {
        obj.pls = obj.content.lastIndexOf( "\n", obj.ls-2 ); obj.pls++;
        obj.previous_line = obj.content.substr( obj.pls, obj.ls-2-obj.pls );
      }
      // Get the next line (next_line)
      obj.next_line = '';
      if ( obj.le != obj.content.length ) {
        obj.nle = obj.content.indexOf( "\n", obj.le + 1 );
        obj.next_line = obj.content.substr( obj.le + 1, obj.nle-obj.le );
      }
      obj.ss -= obj.ls;
      obj.se -= obj.ls;
      return obj;
    };

    var get_jira_stuff = function( content ) {
      var matches = content.match( /{{[A-Z]+\-\d+}}/g );
      if ( ! matches ) {
        return content;
      }
      $.each( matches, function( i, val ) {
        var issue_nr = val.substr( 2, val.length - 4 );
        $.get( 'jira-checker.php?issue='+issue_nr, function( summary ) {
          issues[issue_nr] = $.trim( summary );
          setTimeout( function() {
            // Update the placeholder value with real content
            var content = textarea.val();
            content = get_jira_stuff( content );
            updated_hours( content );
            setTimeout( function() {
              textarea.focus();
            }, 50 );
          }, 1500 );
        } );
        if ( issues[issue_nr] ) {
          content = content.replace( val, issues[issue_nr] );
        }
      } );
      return content;
    }

    var update_text = function( ac, lc ) {
      var new_content = lc.content.substr( 0, lc.ls ) + ac.line + lc.content.substr( lc.le );
      new_content = get_jira_stuff( new_content );
      textarea.val( new_content );
      textarea.get(0).selectionStart = lc.ls + ac.ss;
      textarea.get(0).selectionEnd   = lc.ls + ac.se;
      update_output( new_content );
      setTimeout( function() {
        textarea.focus();
      }, 50 );
    };

    // Do some magic autocompletion & cetera
    textarea.on( 'keydown', function( e ) {
      lc = get_line_context();
      if ( 40 == e.keyCode ) {
        move_line( -1, e ); // Up
      }
      else if ( 38 == e.keyCode ) {
        move_line( 1, e ); // Up
      }
      else if ( 9 == e.keyCode ) {
        e.preventDefault();
        // Line context
        // Autocomplete
        ac = autocomplete( lc.line, lc );
        if ( !ac )
          return;
        update_text( ac, lc );
      }
      context_awareness( lc, textarea );
    } );

    var schedule_blur;
    textarea.on( 'focus blur', function () {
      if ( ! textarea.is(':focus') ) {
        schedule_blur = setTimeout( toggle_blur, 300000 );
      }
      else if ( schedule_blur ) {
        schedule_blur = clearTimeout( schedule_blur );
      }
      else {
        toggle_blur();
      }
    } );
    var toggle_blur = function() {
      $( 'body' ).toggleClass( 'focused', textarea.is(':focus') );
      schedule_blur = false;
    };
  } );

</script>
</body>
</html>
<?php
