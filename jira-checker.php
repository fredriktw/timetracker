<?php

$issue = false;
if ( isset( $_GET['issue'] ) ) {
  $issue = $_GET['issue'];
} else {
  $issue = @$argv[1];
}
if ( ! $issue ) {
  die(1);
}

$config = json_decode( file_get_contents( __DIR__.'/config.json' ), true );

$json = file_get_contents( "https://{$config['username']}:{$config['password']}@jira.driv.digital/rest/api/latest/issue/$issue" );
$data = json_decode( $json, 1 );

echo $data['fields']['summary']."\n";
