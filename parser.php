<?php

$path = __DIR__.'/hours/'. date( 'Y' );
if ( ! file_exists( $path ) ) {
  `mkdir -p "$path"`;
}
$file = $path .'/'. date('W') .'.txt';
if ( @$argv[1] ) {
  $file = $argv[1];
}

if ( !file_exists( $file ) )
  die( $file. ' does not exist!'."\n" );

$content = file_get_contents( $file );

$lines = explode( "\n", $content );
$dates = [];

$date = '';
// Group by date
foreach ( $lines as $line ) {
  $line = trim( $line );
  if ( preg_match( '/^\d{4}\-\d{2}\-\d{2}$/', $line ) ) {
    $date = $line;
    $dates[$date] = [
      'lines' => [],
      'projects' => []
    ];
  }
  elseif ( $line && $date ) {
    $dates[$date]['lines'][] = $line;
  }
}

// var_dump( $dates );

foreach ( $dates as $date => &$data ) {
  $data['projects'] = parse_projects( $data['lines'] );
}
unset( $data );

function parse_projects( $lines ) {
  $projects = [];
  $all_data = json_decode( file_get_contents( 'projects.json' ), 1 );

  $allowed_projects = [];
  foreach ( $all_data['allowed_projects'] as $key => $project_data ) {
    if ( is_array( $project_data ) ) {
      $allowed_projects[ $key ] = $project_data['name'];
    } else {
      $allowed_projects[ $key ] = $project_data;
    }
  }

  $allowed_types    = $all_data['allowed_types'];
  $special_projects = $all_data['special_projects'];
  $special_types    = array_keys( $special_projects );

  foreach ( $special_projects as $project_id => $project ) {
    $allowed_projects[$project_id] = $project;
  }

  foreach ( $lines as $line ) {
    if ( !preg_match( '/^(\d{4})\-(\d{4})\s+(\S+)(\s+\S+)?/', $line, $matches ) )
      continue;

    // Remove matches from the line
    $line = trim( substr( $line, strlen( $matches[0] ) ) );

    // Calculate the time spent
    $to   = hour_to_float( $matches[2] );
    $from = hour_to_float( $matches[1] );
    $time = $to - $from;

    // What project and what type is this?
    $project = strtolower( $matches[3] );
    $type    = trim(@$matches[4]);

    if ( !isset( $allowed_types[$type] ) ) {
      // Type is not set, prepend to the line
      $line = $type .' '. $line;
      $type = $allowed_types['p'];
    }
    else {
      $type = $allowed_types[$type];
    }

    // If it is a special project, skip the type
    if ( in_array( $project, $special_types ) )
      $type = '';

    if ( !isset( $allowed_projects[$project] ) ) {
      // Project is not set, prepend to the line
      $line = $matches[3] .' '. $line;
      $type = '';
      $project = 'Interne prosjekter';
    }
    else {
      $project = $allowed_projects[$project];
    }

    if ( ! $project ) {
      $project = '';
    }
    // echo "project: $project\n type: $type\n line: $line\n";

    if ( ! is_string( $project ) ) {
      print_r( $project );die;
    }

    if ( ! isset( $projects[ $project ] ) ) {
      $projects[$project] = [];
    }
    if ( !isset( $projects[$project][$type] ) )
      $projects[$project][$type] = [];

    $projects[$project][$type][] = ['time' => $time, 'description' => $line];
  }
  return $projects;
}

function nice_time( $time ) {
  $hours = floor($time);
  $minutes = floor( ($time * 60) % 60);
  $out = '';
  if ( $hours )
    $out .= $hours.'t ';
  if ( $minutes )
    $out .= $minutes.'m ';
  return trim( $out );
}
function hour_to_float( $hours ) {
  return substr( $hours, 0, 2 ) + ( substr( $hours, 2 ) / 60 );
}

$my_hours = [];
$template = "{
  \"project\": \"%s\",
  \"type\": \"%s\",
  \"day\": %s,
  \"hours\": %s,
  \"description\": \"%s\"
}";
$template = str_replace( "\n", '', $template );


date_default_timezone_set( 'Europe/Oslo' );

function merge_lines_into( $line, &$new_lines ) {
  foreach ( $new_lines as &$new_line ) {
    if ( $new_line['description'] == $line['description'] ) {
      $new_line['time'] += $line['time'];
      return;
    }
  }
  $new_lines[] = $line;
}

foreach ( $dates as $date => $data ) {
  $timestamp = strtotime( $date );
  $week = intval( date( 'W', $timestamp  ) );
  $day_of_week = date( 'w', $timestamp );
  $day = ( ( 6 + $day_of_week ) % 7 );
  foreach ( $data['projects'] as $project => $activities ) {
    foreach ( $activities as $activity_type => $line_data ) {
      $new_lines = [];
      // var_dump( $line_data );
      $hours = 0;
      foreach ( $line_data as $line_info ) {
        merge_lines_into( $line_info, $new_lines );
        $hours += $line_info['time'];
      }
      $lines = [];
      foreach ( $new_lines as $line_info ) {
        $line = '• ' . addcslashes( $line_info['description'], '"' ). ' '. nice_time( $line_info['time'] );
        $lines[] = $line;
      }
      $hour_line = sprintf(
        $template,
        $project,
        $activity_type,
        $day,
        $hours,
        implode( "\\n", $lines )
      );
      if ( ! isset( $my_hours[$week] ) ) {
        $my_hours[$week] = [];
      }
      $my_hours[$week][] = json_decode( $hour_line, 1 );
      if ( ! end( $my_hours[$week] ) ) {
        print_r([
          $hour_line,
          $template,
          $project,
          $activity_type,
          $day,
          $hours,
          implode( "\\n", $lines )]
        ); die;
      }
    }
  }
}
echo file_get_contents( 'tripletex-inputter.js' )."\n";
echo 'my_hours =' .json_encode( $my_hours, JSON_PRETTY_PRINT ). ";\n";
echo "run_queue( true );\n";
