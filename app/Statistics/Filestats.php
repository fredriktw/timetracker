<?php

/**
 * Filestats
 */
class Filestats {
  public $dir;
  public $time_contstraint = '-cmin -15';
  function __construct( $dir ) {
    if ( '/' != substr( $dir, 0, 1 ) ) {
      throw new Exception( 'Filestats need an absolute path' );
    }
    if ( '/' != substr( $dir, -1 ) ) {
      $dir .= '/';
    }
    $this->dir = str_replace( ' ', '\ ', $dir );
  }
  function exec() {
    $format = 'find %s -type f %s -not \( %s \) -print0 | xargs -0 stat -f \'%%m %%N\'';
    $ignore_dirs = [
      'cli-logs',
      'vendor',
      '.git',
      '.sass-cache',
      'node_modules',
      'var',
      'file-checker',
    ];
    $ignore_lines = [];
    foreach ( $ignore_dirs as $dir ) {
      $ignore_lines[] = sprintf( "-path '*/%s/*'", $dir );
    }
    $command = sprintf(
      $format,
      $this->dir,
      $this->time_contstraint,
      implode(' -o ', $ignore_lines )
    );

    $lines = `$command`;
    return $this->parse_lines( explode( "\n", $lines ) );
  }

  public function parse_lines( $lines ) {
    $new_lines = [];
    foreach ( $lines as $line ) {
      $dir = str_replace( '\ ', ' ', $this->dir );
      $line = str_replace( $dir, '', $line );
      $parts = explode( ' ', $line );
      $parts = array_filter( $parts );
      if ( 2 !== count( $parts ) )
        continue;
      if ( '/' == substr( $line, 0, 1 ) ) {
        $line = substr( $line, 1 );
      }
      $new_lines[$parts[0]] = $line;
    }
    return $new_lines;
  }

}
