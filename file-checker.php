<?php

require_once 'app/Statistics/Filestats.php';
$times = [];
$folders = [
  '/Users/forsvunnet/htdocs/DDP',
  '/www-tools',
  __DIR__,
  '/Users/forsvunnet/cli-shortcuts',
];
foreach ( $folders as $folder ) {
  $filestats = new Filestats( $folder );
  $filestats->time_contstraint = '-cmin -15';
  $nt = $filestats->exec();
  $times =  $times + $nt;
}
ksort( $times );

// var_dump($times);

function check_dir( $path, $sub ) {
  if ( ! file_exists( $path ) ) {
    mkdir( $path );
  }
  foreach ( $sub as $buildup ) {
    $path = "$path$buildup/";
    if ( ! file_exists( $path ) ) {
      mkdir( $path );
    }
    if ( ! file_exists( $path ) ) {
      return false;
    }
  }
  return $path;
}
$dir = check_dir( __DIR__ .'/file-checker/', [
  date( 'Y' ),
  date( '\WW' ),
  date( 'M-d' ),
] );


if ( ! empty( $times ) ) {

  file_put_contents( $dir .time().'.json', json_encode( $times ) );
}
